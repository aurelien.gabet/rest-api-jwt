package main

import (
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"time"
)

var mySigningKey = []byte("jemappelleaurelien")

type credentialsToGetToken struct {
	Name       string `json:"name,omitempty"`
	Password   string `json:"password,omitempty"`
}

type TokenBody struct {
	Token    string `json:"token"`
	Admin    bool   `json:"admin"`
	UserName string `json:"userName"`
	Email    string `json:"email"`
}

type Policies struct {
	Policy    string `json:"policy"`
}

func GenerateJWT(name string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)

	claims := token.Claims.(jwt.MapClaims)

	claims["authorized"] = true
	claims["client"] = name
	claims["exp"] = time.Now().Add(time.Minute * 5).Unix()

	tokenString, err := token.SignedString(mySigningKey)

	if err != nil {
		fmt.Errorf("Something Went Wrong: %s", err.Error())
		return "", err
	}

	return tokenString, nil
}

func isAuthorized(endpoint func(http.ResponseWriter, *http.Request)) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		if r.Header["Authorization"] != nil {

			token, err := jwt.Parse(r.Header["Authorization"][0], func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("There was an error")
				}
				return mySigningKey, nil
			})

			if err != nil {
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
			}

			if token.Valid {
				endpoint(w, r)
			}
		} else {
			http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		}
	})
}

func signin(w http.ResponseWriter, r *http.Request) {

	var credentials credentialsToGetToken
	json.NewDecoder(r.Body).Decode(&credentials)


	token, err := GenerateJWT(credentials.Name)
	if err != nil {
		fmt.Println(err)
	}

	tokenBody := &TokenBody{
		Token:    token,
		Admin:    false,
		UserName: credentials.Name,
		Email:    "aurelien@example.com",
	}

	json.NewEncoder(w).Encode(tokenBody)
}

func policies(w http.ResponseWriter, r *http.Request) {

	policy := &Policies{
		Policy: "ceci est ma policy protégée",
	}

	json.NewEncoder(w).Encode(policy)
}

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/api/v1/auth/token", signin).Methods(http.MethodPost)
	myRouter.Handle("/api/v1/policies", isAuthorized(policies))
	log.Fatal(http.ListenAndServe(":10000", myRouter))
}

func main() {
	handleRequests()
}